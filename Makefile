# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: jmontene <jmontene@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/04/11 14:12:16 by jmontene          #+#    #+#              #
#    Updated: 2017/10/12 15:53:20 by jmontene         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

CC = gcc

CFLAGS = -Wall -Wextra -Werror -O2 ##-fsanitize=address -g3

NAME = wolf3d

_CFILE = main.c raycast.c draw.c keybindings.c \
					mousebindings.c texture_handling.c options.c \
					tools.c minimap.c textureless.c \
					sdlinit.c secret.c bres.c\

LIBDIR = ./libft

LIB_HEADERS = -I libft/includes

LIB_LINK = -L libft/ -lft

CDIR = ./srcs

CFILE = $(patsubst %, $(CDIR)/%, $(_CFILE))

OFILE = $(CFILE:.c=.o)

AUXDIR = $(LIBDIR)

HEADERS = includes

SDL2_PATHS = -rpath @loader_path/frameworks

SDL2_HEADERS = -I frameworks/SDL2.framework/Headers -I frameworks/SDL2_image.framework/Headers -I frameworks/SDL2_ttf.framework/Headers

SDL2_FWORKS = -framework SDL2 -framework SDL2_image -framework SDL2_ttf -F ./frameworks

.PHONY: libft clean fclean re all

all: libft $(NAME)

$(NAME): $(OFILE)
	@echo "Creation Executable $(NAME)............"
	@$(CC) -o $@ $^ $(CFLAGS) -I $(HEADERS) $(LIB_HEADERS) $(LIB_LINK) $(SDL2_PATHS) $(SDL2_FWORKS)
	@echo "DONE"

%.o: %.c
	@echo "Making $@............"
	@$(CC) -o $@ -c $< $(CFLAGS) -I $(HEADERS) $(LIB_HEADERS) $(SDL2_HEADERS)
	@echo "DONE"
	@echo " "

libft:
	@clear
	@echo "Making LibFT............"
	@make -C $(LIBDIR)
	@echo "DONE"
	@echo " "

clean:
	@clear
	@echo "Removing Files.........."
	@echo " "
	@echo " "
	@make -C $(LIBDIR) clean
	@echo "LibFT : DONE"
	@echo " "
	@rm -rf $(OFILE)
	@echo "$(OFILE) : DONE"
	@echo " "
	@sleep 0.1

fclean: clean
	@make -C $(LIBDIR) fclean
	@echo "libft.a : DONE"
	@echo " "
	@rm -rf $(NAME)
	@echo "$(NAME) : DONE"

re: fclean all
