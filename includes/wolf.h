/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   wolf.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmontene <jmontene@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/13 10:28:25 by jmontene          #+#    #+#             */
/*   Updated: 2017/10/13 14:17:58 by jmontene         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef WOLF_H
# define WOLF_H

# include <SDL.h>
# include <SDL_image.h>
# include <SDL_ttf.h>
# include <time.h>
# include <math.h>
# include "libft.h"

# define SCREEN_WIDTH 1280
# define SCREEN_HEIGHT 720
# define BASE_POSX 10.
# define BASE_POSY 10.
# define BASE_DIRX -1.
# define BASE_DIRY 0.
# define BASE_PLANEX 0
# define BASE_PLANEY 0.66
# define TEXT_WIDTH 128
# define TEXT_HEIGHT 128
# define FLTEXT_WIDTH 32
# define FLTEXT_HEIGHT 32
# define CETEXT_WIDTH 1024
# define CETEXT_HEIGHT 1024
# define SECRET_WIDTH 64
# define SECRET_HEIGHT 64
# define MAP_WIDTH 120
# define MAP_HEIGHT 120
# define MAP_START 20
# define MAP_STEP 5
# define MAP_BG 0xA0BFBFBF
# define MAP_FLOOR 0xA0000080
# define MAPWIDTH 24
# define MAPHEIGHT 24
# define SQUARE_PER_SECOND 3.
# define RAD_PER_SECOND 3.
# define WALLTEXT_NB 7
# define BASE_BLUR 1.
# define FONT "fonts/Jersey_M54.ttf"
# define SDL_WHITE (SDL_Color){255,255,255,128}
# define SDL_FPS_COLOR (SDL_Color){255,0,0,128}
# define NUM_OFFSET 30
# define MENU_FADEOUT 3

typedef	enum		e_bool
{
	false,
	true
}					t_bool;

enum				e_walltexture
{
	WALL1,
	WALL2,
	WALL3,
	WALL4,
	WALL5,
	WALL6,
	WALL7,
	WALL8,
	WALL9,
	WALL_TOTAL
};

enum				e_secretframes
{
	FRAME1,
	FRAME2,
	FRAME3,
	FRAME4,
	FRAME5,
	FRAME_TOTAL
};

enum				e_options
{
	OPT_MOVSPEED,
	OPT_ROTSPEED,
	OPT_BLUR,
	OPT_TOTAL
};

typedef struct		s_env
{
	SDL_Window		*win;
	SDL_Surface		*surf;
	SDL_Surface		*drawsurf;
	SDL_Surface		*mmap;
	SDL_Surface		*walltext[WALL_TOTAL];
	SDL_Surface		*ceiling;
	SDL_Surface		*floor;
	SDL_Surface		*text;
	SDL_Surface		*fps;
	SDL_Surface		*secret[FRAME_TOTAL];
	SDL_Event		event;
	TTF_Font		*font;
	Uint32			color;
	Uint32			defcolor;
	Uint32			rmask;
	Uint32			gmask;
	Uint32			bmask;
	Uint32			amask;
	int				imgflags;
	int				updatemap;
	int				updatepos;
	int				currentmenu;
	int				showwalls;
	int				showfps;
	int				showtextures;
	int				framenumber;
	const Uint8		*keystate;
	double			squaresecond;
	double			radsecond;
	double			blur;
	double			frametime;
	double			elapsed;
	double			zbuffer[SCREEN_WIDTH];
	t_bool			quit;
	clock_t			menufadeout;
}					t_env;

typedef struct		s_rc
{
	double			posx;
	double			posy;
	double			dirx;
	double			diry;
	double			planex;
	double			planey;
	double			camerax;
	double			rayposx;
	double			rayposy;
	double			raydirx;
	double			raydiry;
	double			prevdirx;
	double			prevplanex;
	double			sidedistx;
	double			sidedisty;
	double			deltadistx;
	double			deltadisty;
	double			perpwalldist;
	double			wallhit;
	double			xflwall;
	double			yflwall;
	double			xceilingwall;
	double			yceilingwall;
	double			wallscreendist;
	double			playerscreendist;
	double			currentdist;
	double			weight;
	double			currentflx;
	double			currentfly;
	int				mapx;
	int				mapy;
	int				stepx;
	int				stepy;
	int				hit;
	int				side;
	int				lineheight;
	int				drawstart;
	int				drawend;
	int				x;
	int				texnum;
	int				textline;
	int				texty;
	int				fltextx;
	int				fltexty;
	int				elevoffset;
}					t_rc;

typedef struct		s_coord
{
	int				x;
	int				y;
}					t_coord;

typedef struct		s_time
{
	double			time;
	double			prevtime;
	double			frame;
	double			movespeed;
	double			rotspeed;
}					t_time;

typedef struct		s_secret
{
	t_coord			pos;
	double			dist;
	double			secretx;
	double			secrety;
	double			inv;
	double			transx;
	double			transy;
	int				secretscreenx;
	int				secretheight;
	int				secretwidth;
	int				drawstartx;
	int				drawstarty;
	int				drawendx;
	int				drawendy;
	int				stripenb;
	int				textline;
	int				texty;
}					t_secret;

typedef struct		s_bres
{
	float			dx;
	float			dy;
	float			sx;
	float			sy;
	float			e;
	float			e2;
}					t_bres;

typedef struct		s_fcoord
{
	double			x;
	double			y;
}					t_fcoord;

static const int	g_worldmap[MAPWIDTH][MAPHEIGHT] =
{
	{ 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 7, 7, 7, 7, 7, 7, 7, 7},
	{ 4, 0, 0, -6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 7},
	{ 4, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7},
	{ 4, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7},
	{ 4, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 7},
	{ 4, 0, 0, 4, 0, 0, 0, 5, 5, 5, 5, 5, 5, 5, 5, 5, 7, 7, 0, 7, 7, 7, 7, 7},
	{ 4, 0, 0, 1, 0, 0, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 7, 0, 0, 0, 7, 7, 7, 1},
	{ 4, 0, 0, 2, 0, 0, 0, 5, 0, 0, 0, 0, 0, 0, 0, 5, 7, 0, 0, 0, 0, 0, 0, 8},
	{ 4, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 1},
	{ 4, 0, 0, 4, 0, 0, 0, 5, 0, 0, 0, 0, 0, 0, 0, 5, 7, 0, 0, 0, 0, 0, 0, 8},
	{ 4, 0, 0, 1, 0, 0, 0, 5, 0, 0, 0, 0, 0, 0, 0, 5, 7, 0, 0, 0, 7, 7, 7, 1},
	{ 4, 0, 0, 2, 0, 0, 0, 5, 5, 5, 5, 0, 5, 5, 5, 5, 7, 7, 7, 7, 7, 7, 7, 1},
	{ 6, 0, 0, 3, 6, 6, 6, 6, 6, 6, 5, 0, 5, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6},
	{ 8, 0, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4},
	{ 6, 0, 0, 1, 6, 6, 0, 6, 6, 6, 5, 0, 5, 6, 6, 6, 6, 6, 6, 6, 6, -6, 6, 6},
	{ 4, 0, 0, 2, 4, 4, 0, 4, 4, 4, 5, 0, 5, 4, 3, 2, 1, 2, 3, 4, 3, -3, 3, 3},
	{ 4, 0, 0, 3, 0, 0, 0, 0, 0, 4, 5, 0, 5, 3, 0, 0, 0, 0, 0, 3, 0, 0, 0, 2},
	{ 4, 0, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0, 5, 2, 0, 0, 5, 0, 0, 2, 0, 0, 0, 2},
	{ 4, 0, 0, 1, 0, 0, 0, 0, 0, 4, 5, 0, 5, 1, 0, 0, 0, 0, 0, 1, 2, 0, 2, 2},
	{ 4, 0, 0, 2, 6, 0, 0, 0, 0, 4, 5, 0, 0, 0, 0, 0, 5, 0, 0, 0, 0, 0, 0, 2},
	{ 4, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 5, 1, 0, 0, 0, 0, 0, 1, 2, 0, 2, 2},
	{ 4, 0, 0, 4, 6, 0, 0, 0, 0, 4, 5, 0, 5, 2, 0, 0, 5, 0, 0, 2, 0, 0, 0, 2},
	{ 4, 0, 0, -6, 0, 0, 0, 0, 0, 4, 5, 0, 5, 3, 0, 0, 0, 0, 0, 3, 0, 0, 0, 2},
	{ 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 1, 5, 1, 4, 3, 2, 1, 2, 3, 4, 3, 3, 3, 3}
};

void				ft_raycastinit(t_rc *rc);
int					ft_sdlinit(t_env *env);
void				ft_raycast(t_rc *rc);
int					ft_drawstuff(t_env *env, t_rc *rc, t_time *tm);
void				ft_keybindings(t_env *env, t_rc *rc, t_time *tm);
void				ft_mousebindings(t_env *env, t_rc *rc, t_time *tm);
int					ft_textureload(t_env *env);
void				ft_menubindings(t_env *env);
int					ft_fadeout(t_env *env);
void				ft_optvalues(t_env *env);
void				ft_init(t_env *env, t_time *tm);
void				ft_updatetext(t_env *env, int index);
int					ft_fadeout(t_env *env);
void				ft_display_minimap(t_env *env);
void				ft_display_pos(t_env *env, t_rc *rc);
void				ft_handle_time(t_env *env, t_time *tm);
void				ft_blitstuff(t_env *env, t_rc *rc, t_time *tm);
void				ft_drawline(t_env *env, t_rc *rc);
void				ft_draw_flcl(t_env *env, t_rc *rc, Uint32 *pixels, int y);
void				ft_draw_wall(t_env *env, t_rc *rc, Uint32 *pixels, int y);
void				ft_secret(t_env *env, t_rc *rc, t_time *tm);
void				ft_draw_minimap_view(t_rc *rc, Uint32 *pixels, int color);
Uint32				*ft_findframe(t_env *env, t_time *tm);
void				ft_clean_surf(SDL_Surface *surf);

#endif
