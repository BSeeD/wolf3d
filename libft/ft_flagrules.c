/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_flagrules.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmontene <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/11 15:14:05 by jmontene          #+#    #+#             */
/*   Updated: 2017/03/11 18:30:56 by jmontene         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int	ft_ignore_sharp(unsigned char conv)
{
	if ((conv == 'c') || (conv == 'd') || (conv == 'i') ||
			(conv == 'n') || (conv == 'p') || (conv == 's') || (conv == 'u'))
		return (1);
	return (0);
}

int	ft_ignore_zero(t_hd *hd)
{
	if (hd->conv == 'n')
		return (1);
	if (((hd->conv == 'd') || (hd->conv == 'i') || (hd->conv == 'o') ||
		(hd->conv == 'u') || (hd->conv == 'x') || (hd->conv == 'X'))
		&& (hd->flags.bits.prec))
		return (1);
	if (hd->flags.bits.minus)
		return (1);
	return (0);
}

int	ft_ignore_minus(unsigned char conv)
{
	if (conv == 'n')
		return (1);
	return (0);
}

int	ft_ignore_plus(unsigned char conv)
{
	if ((conv == 'a') || (conv == 'A') || (conv == 'd') ||
			(conv == 'e') || (conv == 'E') || (conv == 'f') ||
			(conv == 'F') || (conv == 'g') || (conv == 'G') || (conv == 'i'))
		return (0);
	return (1);
}

int	ft_ignore_space(t_hd *hd)
{
	if ((!ft_ignore_plus(hd->conv)) && (!hd->flags.bits.plus))
		return (0);
	return (1);
}
