/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_flags.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmontene <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/11 17:58:18 by jmontene          #+#    #+#             */
/*   Updated: 2017/03/11 18:31:05 by jmontene         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	set_flags(t_hd *hd, char c)
{
	if (c == '#')
		hd->flags.bits.sharp = 1;
	if (c == '-')
		hd->flags.bits.minus = 1;
	if (c == '+')
		hd->flags.bits.plus = 1;
	if (c == ' ')
		hd->flags.bits.space = 1;
	if (c == '0')
		hd->flags.bits.zero = 1;
	if (c == '.')
		hd->flags.bits.prec = 1;
	if (is_modifier(c))
		hd->flags.bits.mod = 1;
}

void	unset_flags(t_hd *hd)
{
	if (ft_ignore_sharp(hd->conv))
		hd->flags.bits.sharp = 0;
	if (hd->conv == 'p')
		hd->flags.bits.sharp = 1;
	if (ft_ignore_zero(hd))
		hd->flags.bits.zero = 0;
	if (ft_ignore_plus(hd->conv))
		hd->flags.bits.plus = 0;
	if (ft_ignore_space(hd))
		hd->flags.bits.space = 0;
	if (ft_ignore_minus(hd->conv))
		hd->flags.bits.minus = 0;
}
