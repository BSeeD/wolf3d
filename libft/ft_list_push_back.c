/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_push_back.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmontene <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/16 17:58:58 by jmontene          #+#    #+#             */
/*   Updated: 2016/11/16 18:00:51 by jmontene         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_list_push_back(t_list **begin_list, void *data)
{
	t_list *temp;

	if (begin_list)
	{
		temp = *begin_list;
		if (temp)
		{
			while (temp->next != NULL)
				temp = temp->next;
			temp->next = ft_create_elem(data);
		}
		else
			*begin_list = ft_create_elem(data);
	}
}
