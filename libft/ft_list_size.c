/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_size.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmontene <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/16 18:02:16 by jmontene          #+#    #+#             */
/*   Updated: 2016/11/16 18:03:03 by jmontene         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_list_size(t_list *begin_list)
{
	t_list	*temp;
	int		i;

	i = 0;
	if (begin_list)
	{
		temp = begin_list;
		if (temp)
		{
			while (temp)
			{
				temp = temp->next;
				i++;
			}
		}
	}
	return (i);
}
