/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memalloc.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmontene <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/10 11:18:53 by jmontene          #+#    #+#             */
/*   Updated: 2016/11/10 12:02:01 by jmontene         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

void		*ft_memalloc(size_t size)
{
	unsigned char	*temp;

	if (!(temp = (unsigned char *)malloc(size)))
		return (0);
	while (size--)
		temp[size] = 0;
	return (temp);
}
