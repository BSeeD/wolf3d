/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmontene <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/07 14:11:55 by jmontene          #+#    #+#             */
/*   Updated: 2017/01/24 16:33:42 by jmontene         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memcpy(void *dst, const void *src, size_t n)
{
	int				i;
	unsigned char	*tempsrc;
	unsigned char	*tempdst;

	tempsrc = (unsigned char *)src;
	tempdst = (unsigned char *)dst;
	i = 0;
	if (!src && !dst)
		return (0);
	while (n)
	{
		tempdst[i] = tempsrc[i];
		i++;
		n--;
	}
	return (tempdst);
}
