/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmontene <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/07 14:15:22 by jmontene          #+#    #+#             */
/*   Updated: 2016/11/09 14:47:07 by jmontene         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memmove(void *dst, const void *src, size_t len)
{
	int				i;
	unsigned char	*tempsrc;
	unsigned char	*tempdst;

	tempsrc = (unsigned char *)src;
	tempdst = (unsigned char *)dst;
	i = 0;
	if (!src && !dst)
		return (0);
	while (len)
	{
		if (tempdst < tempsrc)
			tempdst[i] = tempsrc[i];
		else
			tempdst[len - 1] = tempsrc[len - 1];
		i++;
		len--;
	}
	return (tempdst);
}
