/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_percent.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmontene <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/07 11:53:52 by jmontene          #+#    #+#             */
/*   Updated: 2017/03/11 18:31:45 by jmontene         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int	ft_printf_percent(t_hd *hd, va_list *ap)
{
	int count;

	count = 0;
	(void)ap;
	if ((!hd->flags.bits.minus) && (hd->flags.bits.width))
		while (hd->wvalue > 1)
		{
			write(1, " ", 1);
			hd->wvalue--;
			count++;
		}
	write(1, "%", 1);
	if ((hd->flags.bits.minus) && (hd->flags.bits.width))
		while (hd->wvalue > 1)
		{
			write(1, " ", 1);
			hd->wvalue--;
			count++;
		}
	return (count + 1);
}
