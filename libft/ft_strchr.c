/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmontene <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/07 14:24:01 by jmontene          #+#    #+#             */
/*   Updated: 2017/01/24 20:22:08 by jmontene         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strchr(const char *s, int c)
{
	int		i;
	char	*temps;

	i = 0;
	temps = (char *)s;
	while (temps[i] != '\0')
	{
		if (temps[i] == c)
			return (&temps[i]);
		i++;
	}
	return ((c == '\0') ? &temps[i] : 0);
}
