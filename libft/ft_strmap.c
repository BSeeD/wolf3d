/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmontene <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/10 14:50:03 by jmontene          #+#    #+#             */
/*   Updated: 2016/11/10 15:00:47 by jmontene         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

char		*ft_strmap(char const *s, char (*f)(char))
{
	int		i;
	char	*temp;

	if (s)
	{
		i = ft_strlen(s);
		if (!(temp = (char *)malloc(sizeof(*temp) * (i + 1))))
			return (0);
		temp[i] = 0;
		while (i--)
			temp[i] = f(s[i]);
		return (temp);
	}
	return (0);
}
