/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmontene <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/10 12:20:47 by jmontene          #+#    #+#             */
/*   Updated: 2017/01/25 16:10:05 by jmontene         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

char	*ft_strnew(size_t size)
{
	char *temp;

	if (!(temp = malloc(sizeof(*temp) * (size + 1))))
		return (0);
	while (size)
		temp[size--] = 0;
	temp[size] = '\0';
	return (temp);
}
