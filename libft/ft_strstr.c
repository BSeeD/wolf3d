/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmontene <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/09 14:39:48 by jmontene          #+#    #+#             */
/*   Updated: 2016/11/09 14:41:49 by jmontene         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strstr(const char *big, const char *little)
{
	size_t	i;
	char	*temp;

	temp = (char *)big;
	if (little[0] == '\0')
		return (temp);
	while (ft_strlen(temp) >= ft_strlen(little))
	{
		i = 0;
		while ((temp[i] != '\0') && (temp[i] == little[i]))
			i++;
		if (i == ft_strlen(little))
			return (temp);
		temp++;
	}
	return (0);
}
