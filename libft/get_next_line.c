/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_new.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmontene <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/24 15:16:33 by jmontene          #+#    #+#             */
/*   Updated: 2017/08/08 07:51:05 by jmontene         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static t_cline	*mynodesearch(int fd, t_cline *clines, int next)
{
	t_cline *clcurrent;

	clcurrent = clines;
	while ((clcurrent->next) && (clcurrent->next->fd != fd))
		clcurrent = clcurrent->next;
	if (next)
		return (clcurrent->next);
	return (clcurrent);
}

static int		myread(int fd, t_cline *clines)
{
	char	buff[BUFF_SIZE + 1];
	char	*temp;
	int		ret;

	clines->cline = ft_strnew(0);
	while (!(ft_strchr(clines->cline, '\n')))
	{
		ret = read(fd, buff, BUFF_SIZE);
		if (!ret)
			return (0);
		if (ret == -1)
			return (-1);
		buff[ret] = '\0';
		temp = clines->cline;
		clines->cline = ft_strjoin(temp, buff);
		free(temp);
	}
	return (ret);
}

static void		mycreate_add(int fd, t_cline *clines)
{
	t_cline	*clcurrent;

	clcurrent = mynodesearch(fd, clines, 0);
	if (!clcurrent->next)
	{
		clcurrent->next = (t_cline *)malloc(sizeof(*clcurrent));
		clcurrent->next->cline =
			ft_strsub(clines->cline, 0, ft_strlen(clines->cline));
		clcurrent->next->fd = fd;
		clcurrent->next->next = NULL;
	}
	else
		clcurrent->next->cline =
			ft_strjoin(clcurrent->next->cline, clines->cline);
}

static int		mydisplay(int fd, t_cline *clines)
{
	char	*temp;
	int		i;
	t_cline	*clcurrent;

	i = 0;
	clcurrent = mynodesearch(fd, clines, 1);
	if (ft_strchr(clcurrent->cline, '\n'))
	{
		while (clcurrent->cline[i] != '\n')
			i++;
		clines->cline = ft_strsub(clcurrent->cline, 0, i);
		temp = clcurrent->cline;
		clcurrent->cline =
			ft_strsub(clcurrent->cline, i + 1,
					(ft_strlen(clcurrent->cline) - i));
		free(temp);
		return (1);
	}
	else
	{
		clines->cline = clcurrent->cline;
		clcurrent->cline = ft_strnew(0);
	}
	return (0);
}

int				get_next_line(const int fd, char **line)
{
	static t_cline	*clines;
	int				ret;
	int				ret2;

	if ((fd < 0) || (line == NULL) || (read(fd, NULL, 0) == -1))
		return (-1);
	if (!clines)
	{
		if (!(clines = (t_cline *)malloc(sizeof(*clines))))
			return (0);
		clines->fd = -10;
		clines->next = NULL;
	}
	ret = myread(fd, clines);
	mycreate_add(fd, clines);
	free(clines->cline);
	ret2 = mydisplay(fd, clines);
	*line = ft_strsub(clines->cline, 0, ft_strlen(clines->cline));
	free(clines->cline);
	return (((*line[0] == '\0') && !ret && !ret2) ? 0 : 1);
}
