/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmontene <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/06 15:59:59 by jmontene          #+#    #+#             */
/*   Updated: 2017/04/21 12:58:33 by jmontene         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define FT_PRINTF_H

# define TYPES_NB 7
# define FUNC_NB 16

# define RESET "\x1B[0m"
# define BOLD "\x1B[1m"
# define BOLD_OFF "\x1B[22m"
# define ITALICS "\x1B[3m"
# define ITALICS_OFF "\x1B[23m"
# define UNDERLINE "\x1B[4m"
# define UNDERLINE_OFF "\x1B[24m"
# define INVERSE "\x1B[7m"
# define INVERSE_OFF "\x1B[27m"
# define STRIKETHROUGH "\x1B[9m"
# define STRIKETHROUGH_OFF "\x1B[29m"

# define RED_FOND "\x1B[31m"
# define GREEN_FOND "\x1B[32m"
# define YELLOW_FOND "\x1B[33m"
# define BLUE_FOND "\x1B[34m"
# define MAGENTA_FOND "\x1B[35m"
# define CYAN_FOND "\x1B[36m"
# define RESET_FOND "\x1B[39m"

# define BACK_BLACK "\x1B[40m"
# define BACK_RED "\x1B[41"
# define BACK_GREEN "\x1B[42m"
# define BACK_YELLOW "\x1B[43m"
# define BACK_BLUE "\x1B[44m"
# define BACK_MAGENTA "\x1B[45m"
# define BACK_CYAN "\x1B[46m"
# define BACK_WHITE "\x1B[47m"
# define BACK_RESET "\x1B[49m"

# include "libft.h"
# include <unistd.h>
# include <stdarg.h>

typedef struct			s_bits
{
	char				sharp : 1;
	char				zero : 1;
	char				minus : 1;
	char				plus : 1;
	char				space : 1;
	char				width : 1;
	char				prec : 1;
	char				mod : 1;
}						t_bits;

typedef union			u_flags
{
	unsigned char		value;
	t_bits				bits;
}						t_flags;

typedef struct			s_hd
{
	t_flags				flags;
	int					wvalue;
	int					pvalue;
	unsigned char		mod;
	unsigned char		conv;
	char				*type;
	char				*out;
}						t_hd;

typedef struct			s_types
{
	unsigned char		mod;
	char				*type;
}						t_types;

typedef struct			s_func
{
	unsigned char		id;
	int					(*f)(t_hd *, va_list *);
}						t_func;

int						ft_printf(const char *format, ...);
int						ft_ignore_sharp(unsigned char conv);
int						ft_ignore_zero(t_hd *hd);
int						ft_ignore_minus(unsigned char conv);
int						ft_ignore_plus(unsigned char conv);
int						ft_ignore_space(t_hd *hd);
int						is_conversion(char c);
int						is_modifier(char c);
int						is_flag(char c);
void					deal_modifier(const char *str, t_hd *hd);
void					get_type(t_hd *hd);
char					*apply_itoa(t_hd *hd, va_list *ap);
void					set_flags(t_hd *hd, char c);
void					unset_flags(t_hd *hd);
int						ft_wstrlen(wchar_t *s);
int						ft_wcharlen(wchar_t c);
int						ft_write_null();
int						pre_padding_str(t_hd *hd, int fcount);
int						write_str(t_hd *hd);
int						post_padding_str(t_hd *hd);
int						handle_sharp(t_hd *hd);
int						handle_flag(t_hd *hd, char *str);
int						ft_printf_char(t_hd *hd, va_list *ap);
int						ft_printf_wchar(t_hd *hd, va_list *ap);
int						ft_printf_num(t_hd *hd, va_list *ap);
int						ft_printf_percent(t_hd *hd, va_list *ap);
int						ft_printf_str(t_hd *hd, va_list *ap);
int						ft_printf_wstr(t_hd *hd, va_list *ap);

static const t_types	g_int_typelist[TYPES_NB] =
{
	{'0', "int"},
	{'h', "short int"},
	{'H', "signed char"},
	{'l', "long"},
	{'L', "long long"},
	{'j', "intmax_t"},
	{'z', "ssize_t"},
};

static const t_types	g_typelist[TYPES_NB] =
{
	{'0', "unsigned int"},
	{'h', "unsigned short"},
	{'H', "unsigned char"},
	{'l', "unsigned long"},
	{'L', "unsigned long long"},
	{'j', "uintmax_t"},
	{'z', "size_t"},
};

static const t_func		g_printf_func[FUNC_NB] =
{
	{'s', &ft_printf_str},
	{'S', &ft_printf_wstr},
	{'d', &ft_printf_num},
	{'i', &ft_printf_num},
	{'D', &ft_printf_num},
	{'u', &ft_printf_num},
	{'U', &ft_printf_num},
	{'o', &ft_printf_num},
	{'O', &ft_printf_num},
	{'x', &ft_printf_num},
	{'X', &ft_printf_num},
	{'c', &ft_printf_char},
	{'C', &ft_printf_wchar},
	{'p', &ft_printf_num},
	{'b', &ft_printf_num},
	{'%', &ft_printf_percent}
};

#endif
