/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bres.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmontene <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/11 10:16:23 by jmontene          #+#    #+#             */
/*   Updated: 2017/10/11 10:24:10 by jmontene         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

static int	ft_linefinished(t_fcoord orig, t_fcoord current, t_fcoord next)
{
	if (orig.x > next.x)
		if (current.x < next.x)
			return (1);
	if (orig.x < next.x)
		if (current.x > next.x)
			return (1);
	if (orig.y > next.y)
		if (current.y < next.y)
			return (1);
	if (orig.y < next.y)
		if (current.y > next.y)
			return (1);
	return (0);
}

void		ft_draw_minimap_view(t_rc *rc, Uint32 *pixels, int color)
{
	t_bres		bres;
	t_fcoord	orig;
	t_fcoord	current;
	t_fcoord	next;

	current.x = MAP_STEP * rc->posx + MAP_START - MAP_STEP;
	current.y = MAP_START + MAP_HEIGHT - MAP_STEP * rc->posy - MAP_STEP;
	next.x = MAP_STEP * rc->posx + 10. * rc->dirx + MAP_START - MAP_STEP;
	next.y = MAP_START + MAP_HEIGHT - MAP_STEP * rc->posy
		- 10. * rc->diry - MAP_STEP;
	bres.dx = fabs(next.x - current.x);
	bres.dy = fabs(next.y - current.y);
	bres.sx = (current.x < next.x ? 1 : -1);
	bres.sy = (current.y < next.y ? 1 : -1);
	bres.e = (bres.dx > bres.dy ? bres.dx : -bres.dy) / 2;
	orig = current;
	while (!ft_linefinished(orig, current, next))
	{
		pixels[(int)current.y * SCREEN_WIDTH + (int)current.x] = color;
		bres.e2 = bres.e;
		if (bres.e2 > -bres.dx)
			((bres.e -= bres.dy) || 1) ? current.x += bres.sx : (void)bres.e;
		if (bres.e2 < bres.dy)
			((bres.e += bres.dx) || 1) ? current.y += bres.sy : (void)bres.e;
	}
}
