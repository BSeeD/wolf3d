/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmontene <jmontene@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/17 17:20:09 by jmontene          #+#    #+#             */
/*   Updated: 2017/10/06 14:39:11 by jmontene         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

void	ft_drawtextline(t_env *env, t_rc *rc)
{
	Uint32	*pixels;
	int		y;

	y = rc->drawstart - 1;
	pixels = env->drawsurf->pixels;
	if (rc->drawend < 0)
		rc->drawend = SCREEN_HEIGHT;
	while (++y < SCREEN_HEIGHT)
	{
		if ((y >= rc->drawstart) && (y <= rc->drawend))
			ft_draw_wall(env, rc, pixels, y);
		else
			ft_draw_flcl(env, rc, pixels, y);
	}
}

void	ft_solve_side(t_rc *rc)
{
	if (rc->side == 0 && rc->raydirx > 0)
		rc->textline = TEXT_WIDTH - rc->textline - 1;
	if (rc->side == 1 && rc->raydiry < 0)
		rc->textline = TEXT_WIDTH - rc->textline - 1;
	if (rc->side == 0 && rc->raydirx > 0)
	{
		rc->xflwall = rc->mapx;
		rc->yflwall = rc->mapy + rc->wallhit;
	}
	else if (rc->side == 0 && rc->raydirx < 0)
	{
		rc->xflwall = rc->mapx + 1.0;
		rc->yflwall = rc->mapy + rc->wallhit;
	}
	else if (rc->side == 1 && rc->raydiry > 0)
	{
		rc->xflwall = rc->mapx + rc->wallhit;
		rc->yflwall = rc->mapy;
	}
	else
	{
		rc->xflwall = rc->mapx + rc->wallhit;
		rc->yflwall = rc->mapy + 1.0;
	}
}

void	ft_solvedraw(t_rc *rc)
{
	if (rc->side == 0)
		rc->perpwalldist = (rc->mapx - rc->rayposx
				+ (1 - rc->stepx) / 2) / rc->raydirx;
	else
		rc->perpwalldist = (rc->mapy - rc->rayposy
				+ (1 - rc->stepy) / 2) / rc->raydiry;
	rc->lineheight = (int)(SCREEN_HEIGHT / rc->perpwalldist);
	rc->drawstart = (-rc->lineheight / 2 + SCREEN_HEIGHT / 2);
	if (rc->drawstart < 0)
		rc->drawstart = 0;
	rc->drawend = (rc->lineheight / 2 + SCREEN_HEIGHT / 2);
	if (rc->drawend > SCREEN_HEIGHT)
		rc->drawend = SCREEN_HEIGHT - 1;
	rc->texnum = abs(g_worldmap[rc->mapx][rc->mapy]) - 1;
	if (rc->side == 0)
		rc->wallhit = rc->rayposy + rc->perpwalldist * rc->raydiry;
	else
		rc->wallhit = rc->rayposx + rc->perpwalldist * rc->raydirx;
	rc->wallhit -= floor(rc->wallhit);
	rc->textline = (int)(rc->wallhit * (double)TEXT_WIDTH);
	ft_solve_side(rc);
	rc->wallscreendist = rc->perpwalldist;
	rc->playerscreendist = 0.0;
}

int		ft_drawstuff(t_env *env, t_rc *rc, t_time *tm)
{
	SDL_FillRect(env->drawsurf, NULL, 0x000000);
	rc->x = -1;
	while (++rc->x < SCREEN_WIDTH)
	{
		ft_raycast(rc);
		ft_solvedraw(rc);
		if (env->showtextures > 0)
			ft_drawtextline(env, rc);
		else
			ft_drawline(env, rc);
		env->zbuffer[rc->x] = rc->perpwalldist;
	}
	ft_display_minimap(env);
	ft_handle_time(env, tm);
	ft_blitstuff(env, rc, tm);
	ft_display_pos(env, rc);
	return (1);
}
