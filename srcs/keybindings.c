/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   keybindings.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmontene <jmontene@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/25 16:42:02 by jmontene          #+#    #+#             */
/*   Updated: 2017/10/13 14:17:01 by jmontene         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

void	ft_key_misc(t_env *env, t_time *tm)
{
	if (env->keystate[SDL_SCANCODE_ESCAPE])
		env->quit = true;
	if (env->keystate[SDL_SCANCODE_0])
	{
		ft_init(env, tm);
		env->menufadeout = clock() / CLOCKS_PER_SEC;
		ft_clean_surf(env->text);
		env->text =
			TTF_RenderText_Solid(env->font, "BACK TO DEFAULT", SDL_WHITE);
		if (!env->text)
		{
			ft_printf("Text surface could not be created! ");
			ft_printf("SDL_Error : %s\n", SDL_GetError());
		}
	}
}

void	ft_key_updown(t_env *env, t_rc *rc, t_time *tm)
{
	if (env->keystate[SDL_SCANCODE_W])
	{
		if (g_worldmap[(int)(rc->posx + rc->dirx * tm->movespeed)]
				[(int)(rc->posy)] <= 0)
			rc->posx += rc->dirx * tm->movespeed;
		if (g_worldmap[(int)(rc->posx)]
				[(int)(rc->posy + rc->diry * tm->movespeed)] <= 0)
			rc->posy += rc->diry * tm->movespeed;
	}
	if (env->keystate[SDL_SCANCODE_S])
	{
		if (g_worldmap[(int)(rc->posx - rc->dirx * tm->movespeed)]
				[(int)(rc->posy)] <= 0)
			rc->posx -= rc->dirx * tm->movespeed;
		if (g_worldmap[(int)(rc->posx)]
				[(int)(rc->posy - rc->diry * tm->movespeed)] <= 0)
			rc->posy -= rc->diry * tm->movespeed;
	}
}

void	ft_key_leftright(t_env *env, t_rc *rc, t_time *tm)
{
	if (env->keystate[SDL_SCANCODE_A])
	{
		if (g_worldmap[(int)(rc->posx - rc->planex * tm->movespeed)]
				[(int)(rc->posy)] <= 0)
			rc->posx -= rc->planex * tm->movespeed;
		if (g_worldmap[(int)(rc->posx)]
				[(int)(rc->posy - rc->planey * tm->movespeed)] <= 0)
			rc->posy -= rc->planey * tm->movespeed;
	}
	if (env->keystate[SDL_SCANCODE_D])
	{
		if (g_worldmap[(int)(rc->posx + rc->planex * tm->movespeed)]
				[(int)(rc->posy)] <= 0)
			rc->posx += rc->planex * tm->movespeed;
		if (g_worldmap[(int)(rc->posx)]
				[(int)(rc->posy + rc->planey * tm->movespeed)] <= 0)
			rc->posy += rc->planey * tm->movespeed;
	}
}

void	ft_keybindings(t_env *env, t_rc *rc, t_time *tm)
{
	ft_key_misc(env, tm);
	ft_key_updown(env, rc, tm);
	ft_key_leftright(env, rc, tm);
}

void	ft_menubindings(t_env *env)
{
	int		i;
	char	*tamer;

	i = -1;
	while (++i < OPT_TOTAL)
		if (env->keystate[i + NUM_OFFSET])
		{
			ft_updatetext(env, env->currentmenu = i);
			env->menufadeout = clock() / CLOCKS_PER_SEC;
		}
	if (env->showfps > 0)
	{
		ft_clean_surf(env->fps);
		env->fps = TTF_RenderText_Solid(env->font,
				tamer = ft_itoa(env->frametime), SDL_FPS_COLOR);
		free(tamer);
	}
}
