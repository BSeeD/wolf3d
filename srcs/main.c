/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmontene <jmontene@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/10 21:29:55 by jmontene          #+#    #+#             */
/*   Updated: 2017/10/13 14:18:13 by jmontene         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

void	ft_freesurfaces(t_env *env)
{
	int i;

	i = -1;
	ft_clean_surf(env->surf);
	ft_clean_surf(env->drawsurf);
	ft_clean_surf(env->mmap);
	while (++i < WALL_TOTAL)
		ft_clean_surf(env->walltext[i]);
	i = -1;
	while (++i < FRAME_TOTAL)
		ft_clean_surf(env->secret[i]);
	ft_clean_surf(env->ceiling);
	ft_clean_surf(env->floor);
	ft_clean_surf(env->text);
	ft_clean_surf(env->fps);
}

void	ft_freestuff(t_env *env, t_rc *rc, t_time *tm)
{
	TTF_CloseFont(env->font);
	env->font = NULL;
	SDL_DestroyWindow(env->win);
	env->win = NULL;
	TTF_Quit();
	IMG_Quit();
	SDL_Quit();
	free(env);
	free(rc);
	free(tm);
}

void	ft_init(t_env *env, t_time *tm)
{
	env->color = 0xFF000000;
	env->defcolor = 0xFF000000;
	env->quit = false;
	env->squaresecond = SQUARE_PER_SECOND;
	env->radsecond = RAD_PER_SECOND;
	env->blur = BASE_BLUR;
	tm->time = (double)clock() / (double)CLOCKS_PER_SEC;
	env->menufadeout = 0;
	env->currentmenu = -1;
	env->showwalls = -1;
	env->showfps = -1;
	env->showtextures = -1;
	env->elapsed = 0;
	env->framenumber = 0;
	ft_clean_surf(env->text);
	ft_clean_surf(env->fps);
	env->text = TTF_RenderText_Solid(env->font,
			"", SDL_WHITE);
	env->fps = TTF_RenderText_Solid(env->font,
			"", SDL_FPS_COLOR);
}

void	ft_gameloop(t_env *env, t_rc *rc, t_time *tm)
{
	ft_keybindings(env, rc, tm);
	while (SDL_PollEvent(&env->event) != 0)
	{
		if (env->event.type == SDL_QUIT)
			env->quit = true;
		else if (env->event.type == SDL_KEYDOWN)
			ft_optvalues(env);
		else if (env->event.type == SDL_MOUSEMOTION)
			ft_mousebindings(env, rc, tm);
	}
	ft_menubindings(env);
	ft_drawstuff(env, rc, tm);
	SDL_UpdateWindowSurface(env->win);
}

int		main(void)
{
	t_env	*env;
	t_rc	*rc;
	t_time	*tm;

	if (!(env = malloc(sizeof(*env))))
		return (0);
	env->text = NULL;
	env->fps = NULL;
	if (!(rc = malloc(sizeof(*rc))))
		return (0);
	if (!(tm = malloc(sizeof(*tm))))
		return (0);
	if (ft_sdlinit(env))
		return (0);
	ft_init(env, tm);
	if (ft_textureload(env))
		return (0);
	ft_raycastinit(rc);
	SDL_SetRelativeMouseMode(SDL_TRUE);
	while (!env->quit)
		ft_gameloop(env, rc, tm);
	ft_freesurfaces(env);
	ft_freestuff(env, rc, tm);
	return (0);
}
