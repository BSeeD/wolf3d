/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minimap.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmontene <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/06 11:48:17 by jmontene          #+#    #+#             */
/*   Updated: 2017/10/11 10:22:46 by jmontene         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

void	ft_draw_minimap_pos(t_fcoord cur, int index,
		Uint32 *pixels, Uint32 color)
{
	pixels[((int)cur.y - index) * SCREEN_WIDTH + (int)cur.x] = color;
	pixels[((int)cur.y - index) * SCREEN_WIDTH + (int)cur.x - 1] = color;
	pixels[((int)cur.y - index) * SCREEN_WIDTH + (int)cur.x - 2] = color;
	pixels[((int)cur.y - index) * SCREEN_WIDTH + (int)cur.x - 3] = color;
	pixels[((int)cur.y - index) * SCREEN_WIDTH + (int)cur.x - 4] = color;
}

void	ft_draw_minimap_pixels(t_coord coord, int index,
		Uint32 *pixels, Uint32 color)
{
	pixels[(MAP_START + MAP_HEIGHT - MAP_STEP * coord.y - MAP_STEP - index)
		* SCREEN_WIDTH + MAP_STEP
		* coord.x + MAP_START - MAP_STEP] = color;
	pixels[(MAP_START + MAP_HEIGHT - MAP_STEP * coord.y - MAP_STEP - index)
		* SCREEN_WIDTH + MAP_STEP
		* coord.x + MAP_START - MAP_STEP - 1] = color;
	pixels[(MAP_START + MAP_HEIGHT - MAP_STEP * coord.y - MAP_STEP - index)
		* SCREEN_WIDTH + MAP_STEP
		* coord.x + MAP_START - MAP_STEP - 2] = color;
	pixels[(MAP_START + MAP_HEIGHT - MAP_STEP * coord.y - MAP_STEP - index)
		* SCREEN_WIDTH + MAP_STEP
		* coord.x + MAP_START - MAP_STEP - 3] = color;
	pixels[(MAP_START + MAP_HEIGHT - MAP_STEP * coord.y - MAP_STEP - index)
		* SCREEN_WIDTH + MAP_STEP
		* coord.x + MAP_START - MAP_STEP - 4] = color;
}

void	ft_display_pos(t_env *env, t_rc *rc)
{
	Uint32		*pixels;
	int			i;
	t_fcoord	coord;

	pixels = env->surf->pixels;
	i = -1;
	coord.x = MAP_STEP * rc->posx + MAP_START - MAP_STEP / 2;
	coord.y = MAP_START + MAP_HEIGHT - MAP_STEP * rc->posy - MAP_STEP / 2;
	ft_draw_minimap_view(rc, pixels, 0xFF00FF00);
	while (++i < MAP_STEP)
		ft_draw_minimap_pos(coord, i, pixels, 0xFFFF0000);
}

void	ft_display_minimap(t_env *env)
{
	Uint32	*pixels;
	t_coord coord;
	int		i;

	pixels = env->mmap->pixels;
	coord.x = -1;
	while (++coord.x < MAPWIDTH)
	{
		coord.y = -1;
		while (++coord.y < MAPHEIGHT)
		{
			i = -1;
			if ((g_worldmap[coord.x][coord.y] > 0)
					|| ((g_worldmap[coord.x][coord.y] < 0)
						&& (env->showwalls < 0)))
				while (++i < MAP_STEP)
					ft_draw_minimap_pixels(coord, i, pixels, MAP_BG);
			if ((g_worldmap[coord.x][coord.y] == 0)
					|| ((g_worldmap[coord.x][coord.y] < 0)
						&& (env->showwalls > 0)))
				while (++i < MAP_STEP)
					ft_draw_minimap_pixels(coord, i, pixels, MAP_FLOOR);
		}
	}
}

int		ft_fadeout(t_env *env)
{
	if ((clock() / CLOCKS_PER_SEC) - env->menufadeout > MENU_FADEOUT)
	{
		env->currentmenu = -1;
		return (1);
	}
	return (0);
}
