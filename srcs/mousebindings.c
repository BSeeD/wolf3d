/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mousebindings.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmontene <jmontene@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/25 16:45:24 by jmontene          #+#    #+#             */
/*   Updated: 2017/10/06 15:06:49 by jmontene         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

void	ft_mouserot(t_rc *rc, t_time *tm, int sign)
{
	rc->prevdirx = rc->dirx;
	rc->dirx = rc->dirx * cos(tm->rotspeed * sign)
		- rc->diry * sin(tm->rotspeed * sign);
	rc->diry = rc->prevdirx * sin(tm->rotspeed * sign)
		+ rc->diry * cos(tm->rotspeed * sign);
	rc->prevplanex = rc->planex;
	rc->planex = rc->planex * cos(tm->rotspeed * sign)
		- rc->planey * sin(tm->rotspeed * sign);
	rc->planey = rc->prevplanex * sin(tm->rotspeed * sign)
		+ rc->planey * cos(tm->rotspeed * sign);
}

void	ft_mousebindings(t_env *env, t_rc *rc, t_time *tm)
{
	if (env->event.motion.xrel < 0)
		while (env->event.motion.xrel++)
			ft_mouserot(rc, tm, 1);
	else if (env->event.motion.xrel > 0)
		while (env->event.motion.xrel--)
			ft_mouserot(rc, tm, -1);
}
