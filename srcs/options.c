/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   options.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmontene <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/06 11:37:05 by jmontene          #+#    #+#             */
/*   Updated: 2017/10/06 14:59:35 by jmontene         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

char	*ft_findtext(t_env *env, int i)
{
	if (i == OPT_MOVSPEED)
		return (ft_strjoin_free("MOVESPEED   ", ft_itoa(env->squaresecond)));
	if (i == OPT_ROTSPEED)
		return (ft_strjoin_free(" ROTSPEED    ", ft_itoa(env->radsecond)));
	if (i == OPT_BLUR)
	{
		return (ft_strjoin_free("  BLUR    ",
					ft_itoa(100. - env->blur * 100.)));
	}
	return (0);
}

void	ft_updatetext(t_env *env, int index)
{
	char *temp;

	temp = ft_findtext(env, index);
	ft_clean_surf(env->text);
	env->text = TTF_RenderText_Solid(env->font,
			temp, SDL_WHITE);
	if (!env->text)
	{
		ft_printf("Text surface could not be created! ");
		ft_printf("SDL_Error : %s\n", SDL_GetError());
	}
	free(temp);
}

double	ft_applyvalues(t_env *env, double value, double step, double max)
{
	if ((env->event.key.keysym.sym == SDLK_MINUS) && (value > 0.01))
	{
		value -= step;
		env->menufadeout = clock() / CLOCKS_PER_SEC;
	}
	if ((env->event.key.keysym.sym == SDLK_EQUALS) && (value <= max))
	{
		value += step;
		env->menufadeout = clock() / CLOCKS_PER_SEC;
	}
	if (value < 0.01)
		value = 0.01;
	if (value > max)
		value = max;
	return (value);
}

double	ft_applyreversevalues(t_env *env, double value, double step, double max)
{
	if ((env->event.key.keysym.sym == SDLK_EQUALS) && (value > 0.01))
	{
		value -= step;
		env->menufadeout = clock() / CLOCKS_PER_SEC;
	}
	if ((env->event.key.keysym.sym == SDLK_MINUS) && (value <= max))
	{
		value += step;
		env->menufadeout = clock() / CLOCKS_PER_SEC;
	}
	if (value < 0.01)
		value = 0.01;
	if (value > max)
		value = max;
	return (value);
}

void	ft_optvalues(t_env *env)
{
	if ((env->currentmenu > -1) && (env->currentmenu < OPT_TOTAL))
	{
		if (env->currentmenu == OPT_MOVSPEED)
			env->squaresecond = ft_applyvalues(env, env->squaresecond, 1., 10.);
		if (env->currentmenu == OPT_ROTSPEED)
			env->radsecond = ft_applyvalues(env, env->radsecond, 1., 10.);
		if (env->currentmenu == OPT_BLUR)
			env->blur = ft_applyreversevalues(env, env->blur, 0.01, 1.);
		ft_updatetext(env, env->currentmenu);
	}
	if (env->event.key.keysym.sym == SDLK_8)
		env->showwalls = -env->showwalls;
	if (env->event.key.keysym.sym == SDLK_f)
		env->showfps = -env->showfps;
	if (env->event.key.keysym.sym == SDLK_t)
		env->showtextures = -env->showtextures;
}
