/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   raycast.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmontene <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/17 17:12:54 by jmontene          #+#    #+#             */
/*   Updated: 2017/10/06 15:34:48 by jmontene         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

void	ft_raycastinit(t_rc *rc)
{
	rc->posx = BASE_POSX;
	rc->posy = BASE_POSY;
	rc->dirx = BASE_DIRX;
	rc->diry = BASE_DIRY;
	rc->planex = BASE_PLANEX;
	rc->planey = BASE_PLANEY;
	rc->elevoffset = 0;
}

void	ft_raycastresolve(t_rc *rc)
{
	rc->camerax = 2 * rc->x / (double)SCREEN_WIDTH - 1;
	rc->rayposx = rc->posx;
	rc->rayposy = rc->posy;
	rc->raydirx = rc->dirx + rc->planex * rc->camerax;
	rc->raydiry = rc->diry + rc->planey * rc->camerax;
	rc->mapx = (int)rc->rayposx;
	rc->mapy = (int)rc->rayposy;
	rc->deltadistx = sqrt(1 + (rc->raydiry * rc->raydiry)
			/ (rc->raydirx * rc->raydirx));
	rc->deltadisty = sqrt(1 + (rc->raydirx * rc->raydirx)
			/ (rc->raydiry * rc->raydiry));
}

void	ft_raycaststepcalc(t_rc *rc)
{
	if (rc->raydirx < 0)
	{
		rc->stepx = -1;
		rc->sidedistx = (rc->rayposx - rc->mapx) * rc->deltadistx;
	}
	else
	{
		rc->stepx = 1;
		rc->sidedistx = (rc->mapx + 1.0 - rc->rayposx) * rc->deltadistx;
	}
	if (rc->raydiry < 0)
	{
		rc->stepy = -1;
		rc->sidedisty = (rc->rayposy - rc->mapy) * rc->deltadisty;
	}
	else
	{
		rc->stepy = 1;
		rc->sidedisty = (rc->mapy + 1.0 - rc->rayposy) * rc->deltadisty;
	}
}

void	ft_raycastwallhit(t_rc *rc)
{
	rc->hit = 0;
	while (rc->hit == 0)
	{
		if (rc->sidedistx < rc->sidedisty)
		{
			rc->sidedistx += rc->deltadistx;
			rc->mapx += rc->stepx;
			rc->side = 0;
		}
		else
		{
			rc->sidedisty += rc->deltadisty;
			rc->mapy += rc->stepy;
			rc->side = 1;
		}
		if (g_worldmap[rc->mapx][rc->mapy] != 0)
			rc->hit = 1;
	}
}

void	ft_raycast(t_rc *rc)
{
	ft_raycastresolve(rc);
	ft_raycaststepcalc(rc);
	ft_raycastwallhit(rc);
}
