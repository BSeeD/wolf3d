/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sdlinit.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmontene <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/06 15:36:33 by jmontene          #+#    #+#             */
/*   Updated: 2017/10/06 15:53:55 by jmontene         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

void	ft_byteorder(t_env *env)
{
	if (SDL_BYTEORDER == SDL_BIG_ENDIAN)
	{
		env->rmask = 0xFF000000;
		env->gmask = 0x00FF0000;
		env->bmask = 0x0000FF00;
		env->amask = 0x000000FF;
	}
	else
	{
		env->rmask = 0x00FF0000;
		env->gmask = 0x0000FF00;
		env->bmask = 0x000000FF;
		env->amask = 0xFF000000;
	}
}

int		ft_surfaces_init(t_env *env)
{
	ft_byteorder(env);
	env->mmap = SDL_CreateRGBSurface(0, SCREEN_WIDTH, SCREEN_HEIGHT,
			32, env->rmask, env->gmask, env->bmask, env->amask);
	if (!env->mmap)
	{
		ft_printf("Minimap could not be created! ");
		ft_printf("SDL_Error : %s\n", SDL_GetError());
		return (0);
	}
	else
	{
		env->drawsurf = SDL_CreateRGBSurface(0, SCREEN_WIDTH, SCREEN_HEIGHT,
				32, env->rmask, env->gmask, env->bmask, env->amask);
		if (!env->drawsurf)
		{
			ft_printf("Buffer surface could not be created! ");
			ft_printf("SDL_Error : %s\n", SDL_GetError());
			return (0);
		}
	}
	return (1);
}

int		ft_misc_init(t_env *env)
{
	env->font = TTF_OpenFont(FONT, 24);
	if (!env->font)
	{
		ft_printf("Font could not be found!\n");
		return (0);
	}
	else
	{
		env->keystate = SDL_GetKeyboardState(NULL);
		env->imgflags = IMG_INIT_PNG;
		if (!(IMG_Init(env->imgflags) && env->imgflags))
		{
			ft_printf("SDL_image could not initialize! ");
			ft_printf("SDL_image Error: %s\n", IMG_GetError());
			return (0);
		}
	}
	return (1);
}

int		ft_sdlinit(t_env *env)
{
	if (SDL_Init(SDL_INIT_EVERYTHING))
		ft_printf("SDL could not initialize! SDL_Error : %s\n", SDL_GetError());
	else
	{
		if (TTF_Init())
		{
			ft_printf("SDL_TTF could not initialize! ");
			ft_printf("SDL_TTF_Error : %s\n", TTF_GetError());
		}
		else
		{
			env->win = SDL_CreateWindow("SonicWolf3D",
					0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, 0);
			if (!env->win)
			{
				ft_printf("Window could not be created! ");
				ft_printf("SDL_Error : %s\n", SDL_GetError());
			}
			else if (ft_surfaces_init(env) && ft_misc_init(env))
				return (0);
		}
	}
	return (-1);
}
