/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   secret.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmontene <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/08 22:05:45 by jmontene          #+#    #+#             */
/*   Updated: 2017/10/11 11:16:22 by jmontene         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

void	ft_findpos_secret(t_secret *secret, t_rc *rc)
{
	secret->pos.x = 11;
	secret->pos.y = 2;
	secret->secretx = secret->pos.x - rc->posx;
	secret->secrety = secret->pos.y - rc->posy;
	secret->inv = 1.0 / (rc->planex * rc->diry - rc->dirx * rc->planey);
	secret->transx = secret->inv
		* (rc->diry * secret->secretx - rc->dirx * secret->secrety);
	secret->transy = secret->inv
		* (-rc->planey * secret->secretx + rc->planex * secret->secrety);
	secret->secretscreenx = (int)((SCREEN_WIDTH / 2)
			* (1 + secret->transx / secret->transy));
	secret->secretheight = abs((int)(SCREEN_HEIGHT / secret->transy));
}

void	ft_find_boundaries(t_secret *secret)
{
	secret->drawstarty = -secret->secretheight / 2 + SCREEN_HEIGHT / 2;
	if (secret->drawstarty < 0)
		secret->drawstarty = 0;
	secret->drawendy = secret->secretheight / 2 + SCREEN_HEIGHT / 2;
	if (secret->drawendy >= SCREEN_HEIGHT)
		secret->drawendy = SCREEN_HEIGHT - 1;
	secret->secretwidth = abs((int)(SCREEN_HEIGHT / (secret->transy)));
	secret->drawstartx = -secret->secretwidth / 2 + secret->secretscreenx;
	if (secret->drawstartx < 0)
		secret->drawstartx = 0;
	secret->drawendx = secret->secretwidth / 2 + secret->secretscreenx;
	if (secret->drawendx >= SCREEN_WIDTH)
		secret->drawendx = SCREEN_WIDTH - 1;
}

void	ft_display_secret(t_secret *secret, t_env *env, Uint32 *secpixels)
{
	int		y;
	double	temp;
	Uint32	*pixels;

	y = secret->drawstarty;
	pixels = env->drawsurf->pixels;
	while (y < secret->drawendy)
	{
		temp = (double)(y - SCREEN_HEIGHT / 2 + secret->secretheight / 2);
		secret->texty = ((temp * SECRET_HEIGHT) / secret->secretheight);
		env->color = secpixels[SECRET_WIDTH * secret->texty + secret->textline];
		if ((env->color & 0xFF000000) != 0)
			pixels[y * SCREEN_WIDTH + secret->stripenb] = env->color;
		y++;
	}
}

void	ft_solve_secret(t_secret *secret, t_env *env, Uint32 *secpixels)
{
	secret->stripenb = secret->drawstartx;
	while (secret->stripenb < secret->drawendx)
	{
		secret->textline = (int)(256
				* (secret->stripenb
				- (-secret->secretwidth / 2 + secret->secretscreenx))
				* SECRET_WIDTH / secret->secretwidth) / 256;
		if (secret->transy > 0
				&& secret->stripenb > 0
				&& secret->stripenb < SCREEN_WIDTH
				&& secret->transy < env->zbuffer[secret->stripenb])
			ft_display_secret(secret, env, secpixels);
		secret->stripenb++;
	}
}

void	ft_secret(t_env *env, t_rc *rc, t_time *tm)
{
	t_secret	secret;
	Uint32		*secpixels;

	secpixels = ft_findframe(env, tm);
	ft_findpos_secret(&secret, rc);
	ft_find_boundaries(&secret);
	ft_solve_secret(&secret, env, secpixels);
}
