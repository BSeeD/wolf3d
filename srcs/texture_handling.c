/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   texture_handling.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmontene <jmontene@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/25 17:19:22 by jmontene          #+#    #+#             */
/*   Updated: 2017/10/06 14:45:11 by jmontene         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

int		ft_textureload2(t_env *env)
{
	int i;

	i = -1;
	env->secret[FRAME1] = IMG_Load("pics/frame1.png");
	env->secret[FRAME2] = IMG_Load("pics/frame2.png");
	env->secret[FRAME3] = IMG_Load("pics/frame3.png");
	env->secret[FRAME4] = IMG_Load("pics/frame4.png");
	env->secret[FRAME5] = IMG_Load("pics/frame5.png");
	while (++i < FRAME_TOTAL)
		if (env->secret[i] == NULL)
			return (-1);
	return (0);
}

int		ft_textureload(t_env *env)
{
	int i;

	i = -1;
	env->walltext[WALL1] = IMG_Load("pics/123.png");
	env->walltext[WALL2] = IMG_Load("pics/201.png");
	env->walltext[WALL3] = IMG_Load("pics/202.png");
	env->walltext[WALL4] = IMG_Load("pics/4.png");
	env->walltext[WALL5] = IMG_Load("pics/160.png");
	env->walltext[WALL6] = IMG_Load("pics/4.png");
	env->walltext[WALL7] = IMG_Load("pics/89.png");
	env->walltext[WALL8] = IMG_Load("pics/55.png");
	env->walltext[WALL9] = IMG_Load("pics/73.png");
	if (!(env->ceiling = IMG_Load("pics/cloud2.jpg")))
		return (-1);
	if (!(env->floor = IMG_Load("pics/grass.jpg")))
		return (-1);
	while (++i < WALL_TOTAL)
		if (env->walltext[i] == NULL)
			return (-1);
	return (ft_textureload2(env));
}

void	ft_draw_flcl(t_env *env, t_rc *rc, Uint32 *pixels, int y)
{
	Uint32	*textpixels;

	rc->currentdist = SCREEN_HEIGHT / (2.0 * y - SCREEN_HEIGHT);
	rc->weight = (rc->currentdist - rc->playerscreendist)
		/ (rc->wallscreendist - rc->playerscreendist);
	rc->currentflx = rc->weight * rc->xflwall
		+ (1.0 - rc->weight) * rc->posx;
	rc->currentfly = rc->weight * rc->yflwall
		+ (1.0 - rc->weight) * rc->posy;
	rc->fltextx = (int)(rc->currentflx * FLTEXT_WIDTH) % FLTEXT_WIDTH;
	rc->fltexty = (int)(rc->currentfly * FLTEXT_HEIGHT) % FLTEXT_HEIGHT;
	textpixels = env->floor->pixels;
	env->color = textpixels[FLTEXT_WIDTH * rc->fltexty + rc->fltextx];
	pixels[y * SCREEN_WIDTH + rc->x] = (env->color & 0x00FFFFFF)
		| ((char)(env->blur * 255) << 24);
	rc->fltextx = (int)(rc->currentflx * CETEXT_WIDTH) % CETEXT_WIDTH;
	rc->fltexty = (int)(rc->currentfly * CETEXT_HEIGHT) % CETEXT_HEIGHT;
	textpixels = env->ceiling->pixels;
	env->color = textpixels[CETEXT_WIDTH * rc->fltexty + rc->fltextx];
	pixels[(SCREEN_HEIGHT - y - 1) * SCREEN_WIDTH + rc->x] =
		(env->color & 0x00FFFFFF) | ((char)(env->blur * 255) << 24);
}

void	ft_draw_wall(t_env *env, t_rc *rc, Uint32 *pixels, int y)
{
	double	temp;
	Uint32	*textpixels;

	textpixels = env->walltext[rc->texnum]->pixels;
	temp = (double)(y - SCREEN_HEIGHT / 2 + rc->lineheight / 2);
	rc->texty = ((temp * TEXT_HEIGHT) / rc->lineheight);
	if (rc->texty > TEXT_HEIGHT - 1)
		rc->texty = TEXT_HEIGHT - 1;
	env->color = textpixels[TEXT_WIDTH * rc->texty + rc->textline];
	if (rc->side == 1)
		env->color = (env->color >> 1) & 8355711;
	pixels[y * SCREEN_WIDTH + rc->x] = (env->color & 0x00FFFFFF)
		| ((char)(env->blur * 255) << 24);
}
