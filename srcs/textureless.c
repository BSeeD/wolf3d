/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   textureless.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmontene <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/06 13:43:45 by jmontene          #+#    #+#             */
/*   Updated: 2017/10/06 13:46:01 by jmontene         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

void	ft_getcolor(t_env *env, t_rc *rc)
{
	env->color = 0xFF000000;
	if (g_worldmap[rc->mapx][rc->mapy] == 1)
		env->color = 0xFFFF0000;
	if (g_worldmap[rc->mapx][rc->mapy] == 2)
		env->color = 0xFF00FF00;
	if (g_worldmap[rc->mapx][rc->mapy] == 3)
		env->color = 0xFF0000FF;
	if (g_worldmap[rc->mapx][rc->mapy] == 4)
		env->color = 0xFFFFFFFF;
	if (g_worldmap[rc->mapx][rc->mapy] == 5)
		env->color = 0xFF00FF00;
	if (g_worldmap[rc->mapx][rc->mapy] == 6)
		env->color = 0xFF0000FF;
	if (g_worldmap[rc->mapx][rc->mapy] == 7)
		env->color = 0xFFFFFFFF;
	if (g_worldmap[rc->mapx][rc->mapy] == 8)
		env->color = 0xFFFF00FF;
	if (rc->side == 1)
		env->color /= 2;
}

void	ft_drawline(t_env *env, t_rc *rc)
{
	Uint32	*pixels;
	int		y;

	ft_getcolor(env, rc);
	y = rc->drawstart - 1;
	pixels = env->drawsurf->pixels;
	if (rc->drawend < 0)
		rc->drawend = SCREEN_HEIGHT;
	while (++y < SCREEN_HEIGHT)
	{
		if ((y >= rc->drawstart) && (y <= rc->drawend))
			pixels[y * SCREEN_WIDTH + rc->x] = (env->color & 0x00FFFFFF)
				| ((char)(env->blur * 255) << 24);
		else
		{
			pixels[y * SCREEN_WIDTH + rc->x] = (env->defcolor & 0x00FFFFFF)
				| ((char)(env->blur * 255) << 24);
			pixels[(SCREEN_HEIGHT - y - 1) * SCREEN_WIDTH + rc->x] =
				(env->defcolor & 0x00FFFFFF) | ((char)(env->blur * 255) << 24);
		}
	}
}
