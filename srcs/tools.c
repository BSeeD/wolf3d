/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tools.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmontene <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/06 13:34:10 by jmontene          #+#    #+#             */
/*   Updated: 2017/10/13 14:17:28 by jmontene         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

void	ft_clean_surf(SDL_Surface *surf)
{
	if (surf)
	{
		SDL_FreeSurface(surf);
		surf = NULL;
	}
}

void	ft_handle_time(t_env *env, t_time *tm)
{
	tm->prevtime = tm->time;
	tm->time = (double)clock() / (double)CLOCKS_PER_SEC;
	tm->frame = tm->time - tm->prevtime;
	tm->movespeed = tm->frame * env->squaresecond;
	tm->rotspeed = tm->frame * env->radsecond / 10;
	env->frametime = 1.0 / tm->frame;
}

void	ft_blitstuff(t_env *env, t_rc *rc, t_time *tm)
{
	SDL_BlitSurface(env->mmap,
			&(SDL_Rect){MAP_START - MAP_STEP, MAP_START - MAP_STEP,
			MAP_START + MAP_WIDTH, MAP_START + MAP_HEIGHT},
			env->drawsurf,
			&(SDL_Rect){MAP_START - MAP_STEP, MAP_START - MAP_STEP,
			MAP_START + MAP_WIDTH, MAP_START + MAP_HEIGHT});
	if (!(ft_fadeout(env)))
		SDL_BlitSurface(env->text, NULL, env->drawsurf,
				&(SDL_Rect){SCREEN_WIDTH - 200, 20, 0, 0});
	if (env->showfps > 0)
		SDL_BlitSurface(env->fps, NULL, env->drawsurf,
				&(SDL_Rect){SCREEN_WIDTH - 50, SCREEN_HEIGHT - 50, 0, 0});
	ft_secret(env, rc, tm);
	env->surf = SDL_GetWindowSurface(env->win);
	SDL_BlitSurface(env->drawsurf, NULL, env->surf, NULL);
}

Uint32	*ft_findframe(t_env *env, t_time *tm)
{
	if ((env->elapsed += tm->frame) > 0.1)
	{
		env->framenumber += 1;
		env->elapsed = 0;
	}
	if (env->framenumber == FRAME_TOTAL)
		env->framenumber = FRAME1;
	return (env->secret[env->framenumber]->pixels);
}
